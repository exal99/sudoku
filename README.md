# Sudoku

![Sudoku](https://bitbucket.org/repo/L7MErx/images/345999103-sudoku-start.png)

Du har säkert sett en sudoku någon gång tidigare: de blev populära för 
ungefär tio år sedan, och nu förtiden kan man hitta dem i i stort sett
varje tidning. 

Reglerna för att lösa en sudoku är enkla: fyll i alla de tomma rutorna
så att varje siffra mellan 1 och 9 bara förekommer en enda gång i varje
rad, kolonn, och 3x3-block.

![Löst sudoku, med alla drag gjorda i rött](https://bitbucket.org/repo/L7MErx/images/617981946-sudoku-done.png)

Din uppgift är att skriva ett program som kan
lösa en given sudoku med hjälp av två enkla metoder.

## Förberedelser

Filen du ska jobba med är `solver.py`. Om du öppnar den ser du att den
redan innehåller en del kod för dig att starta med. 

De delar du behöver bry dig om är funktionen `get_problem(num)`, som hämtar ett problem åt dig (`num` kan vara mellan 0 och 3); `terminal_ui.print_problem(grid)`, som skriver ut problemet som är sparat i `grid`; och `terminal_ui.wait_for_input()` som väntar tills du trycker på någon
tangent (så att du kan se problemet lösas steg för steg). Det finns även
en funktion `terminal_ui.update_problem(row, column, value)` som skriver
ut siffran `value` på platsen som anges av rad 'row' och kolonn 'column'.

För att komma åt siffran som står på en specifik plats i problemet, använd `grid[row][column]`. En nolla innebär att platsen är tom.

Denna modul använder grader, vilket gör det lättare för dig att testa
dina funktioner. Annotera dina funktioner med `@ex(uppgiftsnummer)`
precis ovanför deras definitioner, så kan du testa dem genom att skriva
`grader filnamn.py ex.uppgiftsnummer` i terminalen. 

För att testerna ska
fungera krävs att inargumenten till funktionerna står i en specifik
ordning: först problemet själv, sedan siffran som ska användas,
raden som ska undersökas, och slutligen kolonnen. Alla dessa argument
ska dock inte finnas med i alla funktioner, men exakt vilka som ska
finnas var är det din uppgift att lista ut!

## Enda möjliga siffran

En av de enklaste metoderna för att se vilken siffra som ska stå i en
ruta är att ta fram alla möjliga siffror som kan stå där. Om bara en
enda siffra kan stå i rutan, vet vi att den siffran måste stå där. 

Kom ihåg - en siffra kan bara stå i en ruta om samma siffra inte finns på
någon annan plats i samma rad, kolumn, eller block.

![Det finns bara ett värde som ska förekomma längst upp till vänster](https://bitbucket.org/repo/L7MErx/images/493242989-only-possible-no-4.png)

I exemplet ovan så kan vi komma fram till den enda svaret för rutan längst över till vänster genom att eliminera från startmängden (1, 2, 3, 4, 5, 6, 7, 8, 9) på följande sätt:

* i den översta raden finns redan 1,5,6 så efter de tagits borta kan den bara vara någon av (2,3,4,5,7,8,9)
* i den vänstra raden finns redan 1,3,5,7,9 så efter den återstår (2,4,8)
* i övre vänstra 3x3-rutan finns 1,2,3,5,7,8 så efter det återstår bara 4 som möjligt värde

![4 är den enda möjliga siffran i övre vänstra hörnet](https://bitbucket.org/repo/L7MErx/images/1942070525-only-possible-number.png)



###Uppgift 1.
Skriv en funktion som kontrollerar om en siffra redan finns i en given rad.

###Uppgift 2.
På samma sätt, skriv en funktion som kontrollerar som en siffra finns i
en given kolonn.

###Uppgift 3.
För att kunna kontrollera om en siffra finns i ett block måste vi ha ett
sätt att veta vilket block vi pratar om. Därför numrerar vi blocken i
rader och kolonner på samma sätt som rutorna:

![Block-koordinater](https://bitbucket.org/repo/L7MErx/images/2575818194-block-coords.png)

Skriv en funktion som tar radnumret och kolonnnumret för en ruta och
beräknar radnumret och kolonnnumret för blocket det tillhör.

###Uppgift 4.
Skriv en funktion som beräknar om en siffra finns i ett visst block
(givet med blockkoordinaterna som beskrevs ovan).

###Uppgift 5.
Nu när vi har funktioner för att kontrollera om en siffra finns i en rad,
en kolonn eller ett block, och för att ta fram vilket block en ruta
ligger i, kan vi använda dessa funktioner för att kontrollera om en
siffra skulle kunna sättas in i en viss ruta. Skriv en funktion som gör
detta. (Kom ihåg att man inte kan sätta in någon siffra i en ruta där
det redan står en siffra!)

###Uppgift 6.
Om vi kan kontrollera om en viss siffra kan stå i en viss ruta, går det
lätt att ta fram en lista över alla siffror som skulle kunna stå i rutan.
Skriv en funktion som skapar och returnerar en sådan lista.

###Uppgift 7.
Skriv en funktion som kontrollerar en viss ruta. Om bara ett enda värde
kan stå där ska den returnera det värdet, och annars ska den returnera
`None`.

###Uppgift 8.
Nu har vi alla delar som behövs för att lösa problemet, men det saknas
fortfarande en detalj: vi måste veta när vi är klara. Skriv en funktion
som kontrollerar om problemet är helt ifyllt eller inte.

Nu är det dags att sätta ihop lösaren! På platsen i `solver.py` där det
står "write main loop here", skriv en loop som går igenom hela problemet
ruta för ruta, skriver in en siffra i en ruta om det är den enda siffran
som kan stå där, uppdaterar problemet som visas på skärmen med den nya
siffran, och avslutar när hela problemet är ifyllt. Denna lösare ska
klara av problem 0 från `load_problem`.

##Enda möjliga platsen

En annan av grundmetoderna för att lösa en sudoku är att undersöka om
det finns en rad, kolonn eller ruta där en viss siffra bara kan stå i
en enda ruta. Om detta är fallet kan vi ju veta med säkerhet att siffran
måste stå i den rutan, eftersom det annars skulle finnas en rad, en
kolonn eller ett block där siffran inte kan stå någonstans.

I exemplet nedan kan man se att siffran 4 bara kan stå på en enda plats i den översta raden, dvs andra rutan från vänster i den översta raden eftersom:

![Enda platsen för 4](https://bitbucket.org/repo/L7MErx/images/3192795580-only-place-setup.png)

* i 3:e kolumnen finns redan en 4 på 7:e raden
* i den andra 3x3-rutan från vänster finns redan en 4 
* i den 7:e kolumnen finns redan en 4 på 6:raden


Alltså kan 4 bara sättas i den andra rutan på första raden:

![End platsen](https://bitbucket.org/repo/L7MErx/images/1439704017-only-place.png)

###Uppgift 9.

Skriv en funktion som tar in en siffra och positionen för en ruta och
kontrollerar om denna ruta är den enda ruta i raden som siffran kan
stå i. Funktionen du skrev i uppgift 5 är väldigt användbar här.

###Uppgift 10.

På samma sätt, skriv en funktion som kontrollerar om en ruta är den 
enda ruta i kolonnen där en siffra kan stå.

###Uppgift 11.

Och slutligen, skriv en funktion som gör samma sak fast kontrollerar
inom blocket som innehåller rutan istället.

###Uppgift 12.

Nu ska vi sätta ihop funktionerna från de tre senaste uppgifterna till
en. Skriv en funktion som går igenom alla möjliga siffror som kan stå i
en ruta (här kan funktionen du skrev i uppgift 6 hjälpa till!), och
om någon av dessa bara kan stå i den rutan inom raden, kolonnen eller
boxen, returnera den siffran; annars, returnera `None`.

Lägg nu till sist till din nya funktion i huvudprogrammet, så att om
din förra algoritm (hitta enda möjliga siffran) inte ger ett resultat
för en ruta så ska den nya köras efteråt för samma ruta.
Denna förbättrade lösare klarar också av problem 1, 2 och 3, och många
andra enklare problem. (Om du vill mata in ett eget problem att lösa, 
titta på hur `get_problem` i `assets.py` skapar sina exempelproblem:
du kan skapa egna problem på samma sätt.) Det finns dock många problem
som den inte klarar av; om du är intresserad av de mer avancerade
algoritmer som krävs för at lösa dessa, ta då en titt på
fortsättningsmodulen.